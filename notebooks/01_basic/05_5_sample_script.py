#! /bin/env python3
# El simbolo #! se llama shebang
# Comparar dos valores ingresados desde la linea de comandos e indique su relacion
import argparse

parser = argparse.ArgumentParser(
    description='Compare numbers.',
)

parser.add_argument(
    'numbers', 
    metavar='number', 
    type=float, 
    nargs=2,
    help='An number to compare'
)

arguments = parser.parse_args()

# Unpackaging
a, b = arguments.numbers

if a > b:
    print(f"{a} es mayor que {b}")
elif a < b:
    print(f"{a} es menor que {b}")
else:
    print(f"{a} es igual a {b}")
