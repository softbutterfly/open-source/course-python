#! /bin/env python3
# Comparar dos valores ingresados desde la linea de comandos e indique su relacion
import sys

input_a = sys.argv[1]
input_b = sys.argv[2]

a = float(input_a)
b = float(input_b)

if a > b:
    print(f"{a} es mayor que {b}")
elif a < b:
    print(f"{a} es menor que {b}")
else:
    print(f"{a} es igual a {b}")
