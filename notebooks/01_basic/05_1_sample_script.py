# Leer un valor desde la linea de comandos e imprimir el valor leido 
# y el tipo de dato que tiene
cmd_input = input("Ingresa un valor        : ")

print("Valor ingresado         : ", cmd_input)
print("Tipo del Valor ingresado: ", type(cmd_input))
