#! /bin/env python3
# Comparar dos valores ingresados desde la linea de comandos e indique su relacion
import argparse

parser = argparse.ArgumentParser(
    description='Compare numbers.',
)

parser.add_argument(
    'numbers', 
    metavar='number', 
    type=float, 
    nargs=2,
    help='an integer for the accumulator'
)

def compare(a, b):
    if a > b:
        print(f"{a} es mayor que {b}")
    elif a < b:
        print(f"{a} es menor que {b}")
    else:
        print(f"{a} es igual a {b}")


if __name__ == "__main__":
    arguments = parser.parse_args()
    a, b = arguments.numbers
    compare(a, b)
