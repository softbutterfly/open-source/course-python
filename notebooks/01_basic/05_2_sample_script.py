# Comparar dos valores ingresados desde la linea de comandos e indique su relacion
input_a = input("Ingrese el valor A: ")
input_b = input("Ingrese el valor B: ")

a = float(input_a)
b = float(input_b)

print()
print("Resultado: ")

if a > b:
    print(f"* {a} es mayor que {b}")
elif a < b:
    print(f"* {a} es menor que {b}")
else:
    print(f"* {a} es igual a {b}")
